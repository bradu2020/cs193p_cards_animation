//
//  ViewController2.swift
//  Lecture 8 - Animation
//
//  Created by Michel Deiman on 11/12/2017.
//	Alternative implementation
//  Copyright © 2017 Michel Deiman. All rights reserved.
//

import UIKit
import CoreMotion

struct Constants {
	static var flipCardAnimationDuration: TimeInterval = 0.6
	static var matchCardAnimationDuration: TimeInterval = 0.6
	static var matchCardAnimationScaleUp: CGFloat = 3.0
	static var matchCardAnimationScaleDown: CGFloat = 0.1
	static var behaviorResistance: CGFloat = 0
	static var behaviorElasticity: CGFloat = 1.0
	static var behaviorPushMagnitudeMinimum: CGFloat = 0.5
	static var behaviorPushMagnitudeRandomFactor: CGFloat = 1.0
	static var cardsPerMainViewWidth: CGFloat = 5
}

class ViewController: UIViewController {

	private var deck = PlayingCardDeck()

	@IBOutlet var cardViews: [PlayingCardView]!
	
	private lazy var animator = UIDynamicAnimator(referenceView: view)
	private lazy var cardBehavior = CardBehavior(in: animator)
    
    //Turn on accelerometer when appearing, turn off when dissapearing
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if CMMotionManager.shared.isAccelerometerAvailable {
            //turn on the gravity
            cardBehavior.gravityBehavior.magnitude = 1.0
            
            CMMotionManager.shared.accelerometerUpdateInterval = 1/10 //every 10th of a second
            
            //register closure to call with accelerometer updates
            CMMotionManager.shared.startAccelerometerUpdates(to: .main) { (data, error) in
                //data? because it could be an error, then ignore it (should be adressed)
                if var x = data?.acceleration.x, var y = data?.acceleration.y {
                    
                    //accelerometer data is in respect to the device, hardware and view are not the same
                    //Also rotation affects this, fix:
                    switch UIDevice.current.orientation {
                    case .portrait: y *= -1
                    case .portraitUpsideDown: break
                    case .landscapeRight: swap(&x, &y)
                    case .landscapeLeft: swap(&x, &y); y *= -1
                    default: x = 0; y = 0
                    }
                    //set the direction(vector) of gravity in gravity behavior
                    self.cardBehavior.gravityBehavior.gravityDirection = CGVector(dx: x, dy: y)
                }
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cardBehavior.gravityBehavior.magnitude = 0
        //always stop to save battery
        CMMotionManager.shared.stopAccelerometerUpdates()
    }
	
	override func viewDidLoad() {
		super.viewDidLoad()
		var cards = [PlayingCard]()
		for _ in 1...((cardViews.count + 1)/2) {
			let card = deck.draw()!
			cards += [card, card]
		}
		let cardScaleFactor = (view.bounds.width / Constants.cardsPerMainViewWidth)/cardViews[0].frame.width
		for cardView in cardViews {
			cardView.frame.size.width *= cardScaleFactor
			cardView.frame.size.height *= cardScaleFactor
			cardView.isFaceUp = false
			let card = cards.remove(at: cards.count.arc4Random)
			cardView.rank = card.rank.order
			cardView.suit = card.suit.rawValue
            
            //target could be this VC or the view
			cardView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(flipCard(_:)))) //Name of the method and ONLY external names of args
			cardBehavior.addItem(cardView)
		}
	}
	
	private var faceUpCardViews: [PlayingCardView]  {
		return cardViews.filter { cardView in
			!matchedCardViews.contains(cardView) && cardView.isFaceUp == true
		}
	}
	
	private var faceUpCardViewsMatch: Bool {
		return faceUpCardViews.count == 2 && faceUpCardViews[0] == faceUpCardViews[1]
	}
	
	private var matchedCardViews: Set<PlayingCardView> = []
	private var lastChoosenCardview: PlayingCardView?
	
	@IBAction func flipCard(_ sender: UITapGestureRecognizer) {
		switch sender.state {
		case .ended:
            // view is (a var in tapgeturerecognizer) the part that was tapped on
			if let chosenCardView = sender.view as? PlayingCardView, faceUpCardViews.count < 2 {
				lastChoosenCardview = chosenCardView
				cardBehavior.removeItem(chosenCardView)
				chosenCardView.flipCard(completion: { [unowned self] in
					if self.faceUpCardViewsMatch {
						self.faceUpCardViews.forEach { cardView in
							self.matchedCardViews.insert(cardView)
							cardView.cardsMatchAnimation(completion: nil)
						}
					} else if self.faceUpCardViews.count == 2 && chosenCardView == self.lastChoosenCardview {
						self.faceUpCardViews.forEach { cardView in
							cardView.flipCard(completion: { [unowned self] in
								self.cardBehavior.addItem(cardView)
							})
						}
					} else if !chosenCardView.isFaceUp {
						self.cardBehavior.addItem(chosenCardView)
					}
				})
			}
		default: break
		}
	}
}

