//
//  CMMotionManager+shared.swift
//  Lecture 8 - Animation
//
//  Created by XeVoNx on 08/06/2020.
//  Copyright © 2020 Michel Deiman. All rights reserved.
//

import CoreMotion

extension CMMotionManager {
    //Now anyone wants to use motion manager can use this one
    static var shared = CMMotionManager()
}
